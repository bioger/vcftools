#!/usr/bin/env python

import sys
import getopt
from commons.core.coord.Align import Align

def help():
    print()
    print( "usage: %s [ options ]" % ( sys.argv[0].split("/")[-1] ))
    print( "options:")
    print( "     -h: this help")
    print( "     -i: input file name (format='align')")
    print( "     -o: output file name (format='set', default=inFileName+'.set')")
    print( "     -v: verbosity level (default=0/1)")
    print()
    

def align2set( inFileName, outFileName ):
    alignFileHandler = open( inFileName, "r" )
    setFileHandler = open( outFileName, "w" )
    iAlign = Align()
    countAlign = 0
    while True:
        line = alignFileHandler.readline()
        if line == "":
            break
        countAlign += 1
        iAlign.setFromString( line, "\t" )
        setFileHandler.write( "%i\t%s\t%s\t%i\t%i\n" % ( countAlign,
                                                         iAlign.getSubjectName(),
                                                         iAlign.getQueryName(),
                                                         iAlign.getQueryStart(),
                                                         iAlign.getQueryEnd() ) )
    alignFileHandler.close()
    setFileHandler.close()


def main():

    inFileName = ""
    outFileName = ""
    verbose = 0

    try:
        opts, args = getopt.getopt( sys.argv[1:], "hi:o:v:" )
    except getopt.GetoptError as err:
        print(str(err))
        help()
        sys.exit(1)
    for o,a in opts:
        if o == "-h":
            help()
            sys.exit(0)
        elif o == "-i":
            inFileName = a
        elif o == "-o":
            outFileName = a
        elif o == "-v":
            verbose = int(a)

    if  inFileName == "":
        print("ERROR: missing input file name")
        help()
        sys.exit(1)

    if verbose > 0:
        print("START %s" % ( sys.argv[0].split("/")[-1] ))
        sys.stdout.flush()

    if outFileName == "":
        outFileName = "%s.set" % ( inFileName )

#TODO: move 'align2set' into 'AlignUtils.convertAlignFileIntoPSetFile' with a test
#    AlignUtils.convertAlignFileIntoPSetFile( inFileName, outFileName )

    align2set( inFileName, outFileName )

    if verbose > 0:
        print("END %s" % ( sys.argv[0].split("/")[-1] ))
        sys.stdout.flush()

    return 0


if __name__ == "__main__":
    main()
