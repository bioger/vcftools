# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from commons.core.checker.IChecker import IChecker
from commons.core.LoggerFactory import LoggerFactory


## Enable a Logger in your Checker.
#
#  Subclasses of  AbstractChecker have a already a logger enabled (referenced by self._log attribute). Subclasses also already implements IChecker.
#  All you have to do is to call __init__() method in your own constructor.
class AbstractChecker( IChecker ):
    
    ## Constructor 
    #
    # @param logFileName name of log file where logger outputs
    #
    def __init__(self, logFileName):
        self._log = LoggerFactory.createLogger(logFileName)
        
        
    ## Set (change) default logger
    #
    # @param logger a new logger
    # 
    def setLogger(self, logger):
        self._log = logger
        
        
    ## Return the logger instance
    #
    def getLogger(self):
        return self._log
