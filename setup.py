import os, shutil, glob, re, sys, time
from distutils.util import convert_path
from distutils.core import setup
from distutils.cmd import Command

setup(
      name = "VCFtools",
      version = '1.3',
      description='Set of tools to analyse variants developed for the Gandalf project',
      author='URGI team, Bioinfobioger',
      author_email='urgi-support@versailles.inra.fr, bioinfobioger.inrae.fr',
      url='https://urgi.versailles.inra.fr/Projects/GANDALF',
      scripts=["Gnome_tools/VCFFiltering.py",
               "Gnome_tools/VCFStorage.py",
               "Gnome_tools/VCFCarto.py",
               "Gnome_tools/VCFStorageBGZIP.py",
               "Gnome_tools/VCFCartoMultiPoint.py"],
      packages=['commons','commons/core','commons/core/utils','commons/core/checker',
                'commons/core/seq','commons/core/coord','commons/core/stat'],
      data_files=[('',['Gnome_tools/doc/README_VCFtools.txt']),('',['LICENSE'])],
      )
