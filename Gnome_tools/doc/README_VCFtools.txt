==============================
README for VCFtools
==============================

------------------------------------------------------------
======================== description =======================
------------------------------------------------------------
VCFtools is a package that contains 3 tools : 
 - VCFFiltering

 - VCFStorage
 
 - VCFCarto

------------------------------------------------------------
======================= dependencies =======================
------------------------------------------------------------

no direct dependencies ; but VCFFiltering works only with Freebayes VCF files (Mpileup and Naive Variant Caller not optimally)

------------------------------------------------------------
===================== INSTALLATION =========================
------------------------------------------------------------

$tar -xzf VCFtools-1.2.tar.gz
$cd VCFtools-1.2/
$python setup_VCFtools.py install
$export PYTHONPATH=$PWD
$export PATH=$PATH:$PWD/bin

==============================
VCFFiltering
==============================

------------------------------------------------------------
======================== description =======================
------------------------------------------------------------

VCFFiltering is a python script that allows to filter SNP results from freebayes on multiple criterias as once. The filters are : 

 - Allele number : number of possible allele at the genomic position
 - Allele frequency : frenquency of the most represented allele ; note that if the most represented allele is the reference (a "." in the 4th column of the VCF, the allele frequency will still work but allele frequency should be under 1-x)
 - Depth : Higher and lower bound of the depth ; the depth is the number of reads mapped on the genomic positions.

Depth can be automatically detected. If you do so, The 90 % of the positions with a depth closest to the most frequent depth will pass the filter. 
	
This script has been developped to be used with freebayes output, on haploïd data. 

/!\ the VCF source is detected from the header. Please keep the header of your VCF file if you want to use this tool
------------------------------------------------------------
========================== options =========================
------------------------------------------------------------

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -f VCFFILE, --vcf=VCFFILE
                        Input VCF File name [compulsory] [format: VCF]
  -o OUTFILE, --output=OUTFILE
                        output VCF File name [compulsory] [format: VCF]
  -m MINDP, --minDP=MINDP
                        minimum of depth ; if both minDP and maxDP are set,
                        optimal DP will not be calculated
  -M MAXDP, --maxDP=MAXDP
                        maximum of depth ; if both minDP and maxDP are set,
                        optimal DP will not be calculated
  -N AN, --AN=AN        maximum number of allele for a SNP; default = 2
  -F AF, --AF=AF        minimum frequency for the alternative allele of a SNP;
                        default = 0.9
  -Q QUAL, --qual=QUAL  minimum quality for a SNP
  -b BEDFILES, --bed=BEDFILES
                        bed files: list of coordinates to filter, multiple
                        arguments allowed '-b file1 -b file2'
  -v VERBOSITY, --verbosity=VERBOSITY
                        Verbosity [optional] [default: 3]
  -s, --stats           output data for stats on DP [optional][default: False]


------------------------------------------------------------
===================== command examples =====================
------------------------------------------------------------
example 1 : 
$VCFFiltering.py -f myVCF.vcf -o FilteredVCF.vcf

example 2 : 
$VCFFiltering.py -f myVCF.vcf -N 2 -F 0.87 -m4 -M200 -b bed1.bed bed2.bed -o FilteredVCF.vcf

------------------------------------------------------------
======================== input files =======================
------------------------------------------------------------

input format is a VCF file obtaines with freebayes ; headers are necessary
you can also add some bed files to filter some specific regions.

------------------------------------------------------------
========================== results =========================
------------------------------------------------------------

Output file is a VCF file with filters specified on the 7th column (FILTER). The details on the filters are explained on the header : 

##FILTER=<ID=G_AN,Description="The SNP has been filtered ; out of AN range(over 2)">
##FILTER=<ID=G_AF,Description="The SNP has been filtered ; out of AF range(under 0.9)">
##FILTER=<ID=G_DP,Description="The SNP has been filtered ; out of DP range(3 - 5)">
##FILTER=<ID=InDel,Description="The SNP has been filtered ; InDel detected">
##FILTER=<ID=Nmatch,Description="The SNP has been filtered ; reference base detected : N">

In case of a bed file, extra header lines and extra filters will be added. 

VCFFiltering also adds some INFO on the 8th column (INFO). The details on the infos are explained on the header : 

##INFO=<ID=G_AN,Number=1,Type=Integer,Description="Total number of alleles calculated by VCFFiltering">
##INFO=<ID=G_AF,Number=1,Type=Float,Description="frequency of the most supported alternative allele calculated by VCFFiltering">
##INFO=<ID=G_base,Number=1,Type=String,Description="base of the most supported alternative allele found by VCFFiltering">
##INFO=<ID=G_DP,Number=1,Type=Integer,Description="total depth calculated by VCFFiltering">

The VCF files obtained after VCFFiltering can be used directly for VCFStorage. 


==============================
VCFStorage.py
==============================

------------------------------------------------------------
======================== description =======================
------------------------------------------------------------

VCFStorage allows to store data from multiple VCF files into a single tabular marker file. 

------------------------------------------------------------
========================== options =========================
------------------------------------------------------------

  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -f FASTAFILE, --fasta=FASTAFILE
                        Input fasta file name [compulsory] [format: Fasta]
  -l GENOMELISTFILE, --genomelist=GENOMELISTFILE
                        Input list of genome/vcf file name [compulsory]
                        [format: tab]
  -w WORKDIR, --workDir=WORKDIR
                        name of the workingDirectory
  -o OUTFILE, --out=OUTFILE
                        Output file name [compulsory] [format: tab]
  -c, --clean           Clean temporary files [optional] [default: False]
  -v VERBOSITY, --verbosity=VERBOSITY
                        Verbosity [optional] [default: 3]

------------------------------------------------------------
===================== command examples =====================
------------------------------------------------------------

$VCFStorage -f fasta.fa -l genomelist.list -c -w workdir -o output.tab 

------------------------------------------------------------
======================== input files =======================
------------------------------------------------------------

 - the fasta file of your genomic sequence
 - multiple VCF files (1 per strain). It is strongly advised to use the column filter (col 7) for filtered positions instead of removing the lines from the VCF. All the VCF files should be listed on a tabulated file with 2 columns, the first one containing the strain name and the second one containing the VCF file full path


------------------------------------------------------------
========================== results =========================
------------------------------------------------------------

the result is a tab delimited format file  where all genomic positions are in rows, and all strains are in columns (in the order you gave the VCF)

For each position and each genome, a code is attributed : 

- for the reference : ::

    A,T,G,C for the corresponding nucleotidic acid 

- for the genomes : ::

    U if the position was not refered in the VCF file 
    R if the base is similar to the reference 
    F if the base has been filtered in the column FILTER (column 7) of the VCF 
    A,T,G,C if the genome has a validated SNP at the position
    
    
==============================
VCFCarto.py
==============================

------------------------------------------------------------
======================== description =======================
------------------------------------------------------------

 
VCFcarto can convert a tabulated file into a file with only the SNP from refA and refH. 
VCFcarto is best used with VCFStorage output as input.
2 formats are possible, either the input format is conserved, or the format is changed into a 3 letter format
("A" for refA, "H" for refH and "-" when the base do not correspond to any parent)

------------------------------------------------------------
========================== options =========================
------------------------------------------------------------

  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -f TABLENAME, --file=TABLENAME
                        Input TSV File name [compulsory] [format: TSV]
  -o OUTFILE, --output=OUTFILE
                        output TSV File name [compulsory] [format: TSV]
  -A REFA, --refA=REFA  name of the reference genome A [compulsory]
  -H REFH, --refH=REFH  name of the reference genome H [compulsory]
  -p, --onlyParents     Will change every letters by either A or H depending
                        on which parents the strain correspond to for that
                        base[optional] [default: False]
  -m, --mergeMarkers    Will merge sequential markers with the same
                        information ; option -p is needed [optional] [default:
                        False]
  -s, --statistics      Gives statistics on stdout[optional] [default: False]
  -g, --graphics        create graphs. Only works with -p[optional] [default:
                        False]
  -v VERBOSITY, --verbosity=VERBOSITY
                        Verbosity [optional] [default: 3]

------------------------------------------------------------
===================== command examples =====================
------------------------------------------------------------

example 1 : 
$VCFcarto.py -f Storage.out -A G15 -H G23 -o FilteredStorage.out
example 2 : 
$VCFcarto.py -f Storage.out -A ref1 -H ref2 -p -s -g -m -o cartoTable.out

------------------------------------------------------------
======================== input files =======================
------------------------------------------------------------

expected input format is the output from VCFStorage.

the expected format is a tab delimited format file where all genomic positions are in rows, and all strains are in columns

For each position and each genome, a code is attributed : 

- for the reference : ::

    A,T,G,C for the corresponding nucleotidic acid 

- for the genomes : ::

    U if the position was not refered in the VCF file 
    R if the base is similar to the reference 
    F if the base has been filtered 
    A,T,G,C if the genome has a validated SNP at the position

------------------------------------------------------------
========================== results =========================
------------------------------------------------------------

for the main output, 2 formats are possible : 

- The first format is similar to the input format (same columns and code) but will only be conserved lines where the 2 parents have different alleles. 

- The second format (A - H format) will have a much simpler code ::

    "A" when the strain allele is the same as parent A
    "H" when the strain allele is the same as parent H
    "-" in any other case (base filtered, different base, base unmapped etc...)

the second format may be used as an input for a cartographic tool. 

If you decide to have the A - H format, you can also merge consecutive markers that carries the same information (every strains are similars between the two markers). If you decide to do so, new markers will be generated and a bed file will do the link between the input and the output markers.

Finally, you can also choose to generate a graphical output to visualise the result. 


==============================
working examples 
==============================


if you want to test you scripts, here are full examples with expected results for each tool :

------------------------------------------------------------
======================= VCFFiltering =======================
------------------------------------------------------------

VCF input file: 
---------------

    ##fileformat=VCFv4.1
    ##fileDate=20150126
    ##source=freeBayes v0.9.13-2-ga830efd
    ##reference=ref.fsa
    ##phasing=none
    ##commandline="freebayes --report-monomorphic --ploidy 2 -X -u -f ref.fsa strain_1.bam"
    #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	strain_1
    chrom1	1	.	T	.	.	.	DP=4;DPB=1;EPPR=5.18177;GTI=0;MQMR=36;NS=1;NUMALT=0;ODDS=0;PAIREDR=1;PQR=0;PRO=0;QR=38;RO=4;RPPR=5.18177	GT:DP:RO:QR:AO:QA:GL	0/0:4:4:38:.:.:0
    chrom1	2	.	A	.	.	.	DP=12;DPB=1;EPPR=5.18177;GTI=0;MQMR=36;NS=1;NUMALT=0;ODDS=0;PAIREDR=1;PQR=0;PRO=0;QR=38;RO=11;RPPR=5.18177	GT:DP:RO:QR:AO:QA:GL	0/0:12:11:38:.:.:0
    chrom1	3	.	T	A	.	.	DP=5;DPB=1;EPPR=5.18177;GTI=0;MQMR=36;NS=1;NUMALT=0;ODDS=0;PAIREDR=1;PQR=0;PRO=0;QR=38;AO=5;RPPR=5.18177	GT:DP:RO:QR:AO:QA:GL	0/0:5:0:38:5:.:0
    chrom1	4	.	G	T	.	.	DP=6;DPB=1;EPPR=5.18177;GTI=0;MQMR=36;NS=1;NUMALT=0;ODDS=0;PAIREDR=1;PQR=0;PRO=0;QR=38;AO=5;RPPR=5.18177	GT:DP:RO:QR:AO:QA:GL	0/0:6:1:38:5:.:0
    chrom1	5	.	C	C	.	.	DP=12;DPB=1;EPPR=5.18177;GTI=0;MQMR=36;NS=1;NUMALT=0;ODDS=0;PAIREDR=1;PQR=0;PRO=0;QR=38;AO=11;RPPR=5.18177	GT:DP:RO:QR:AO:QA:GL	0/0:12:1:38:11:.:0

purposed command:
-----------------

$VCFFiltering.py -f myVCF.vcf -N 2 -F 0.9 -m5 -M14 -o FilteredVCF.vcf

which will do : 

 - Calculate optimal depth range automatically = no
 - minumum Depth = 5
 - maximum Depth = 14
 - minimum allele frequency = 0.9
 - maximum allele number = 2

exemple result :
----------------

    ##fileformat=VCFv4.1
    ##fileDate=20150126
    ##source=freeBayes v0.9.13-2-ga830efd
    ##reference=ref.fsa
    ##phasing=none
    ##commandline="freebayes --report-monomorphic --ploidy 2 -X -u -f ref.fsa strain_1.bam"
    #CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	strain_1
    chrom1	1	.	T	.	.	G_DP	DP=4;DPB=1;EPPR=5.18177;GTI=0;MQMR=36;NS=1;NUMALT=0;ODDS=0;PAIREDR=1;PQR=0;PRO=0;QR=38;RO=4;RPPR=5.18177;G_AN=0;G_AF=0.00;G_DP=4;G_Base=T	GT:DP:RO:QR:AO:QA:GL	0/0:4:4:38:.:.:0
    chrom1	2	.	A	.	.	.	DP=12;DPB=1;EPPR=5.18177;GTI=0;MQMR=36;NS=1;NUMALT=0;ODDS=0;PAIREDR=1;PQR=0;PRO=0;QR=38;RO=11;RPPR=5.18177;G_AN=0;G_AF=0.08;G_DP=12;G_Base=A	GT:DP:RO:QR:AO:QA:GL	0/0:12:11:38:.:.:0
    chrom1	3	.	T	A	.	.	DP=5;DPB=1;EPPR=5.18177;GTI=0;MQMR=36;NS=1;NUMALT=0;ODDS=0;PAIREDR=1;PQR=0;PRO=0;QR=38;AO=5;RPPR=5.18177;G_AN=0;G_AF=1.00;G_DP=5;G_Base=A	GT:DP:RO:QR:AO:QA:GL	0/0:5:0:38:5:.:0
    chrom1	4	.	G	T	.	G_AF	DP=6;DPB=1;EPPR=5.18177;GTI=0;MQMR=36;NS=1;NUMALT=0;ODDS=0;PAIREDR=1;PQR=0;PRO=0;QR=38;AO=5;RPPR=5.18177;G_AN=0;G_AF=0.83;G_DP=6;G_Base=T	GT:DP:RO:QR:AO:QA:GL	0/0:6:1:38:5:.:0
    chrom1	5	.	C	C	.	.	DP=12;DPB=1;EPPR=5.18177;GTI=0;MQMR=36;NS=1;NUMALT=0;ODDS=0;PAIREDR=1;PQR=0;PRO=0;QR=38;AO=11;RPPR=5.18177;G_AN=0;G_AF=0.92;G_DP=12;G_Base=C	GT:DP:RO:QR:AO:QA:GL	0/0:12:1:38:11:.:0

------------------------------------------------------------
====================== VCFStorage.py =======================
------------------------------------------------------------

inputs :
--------
	fasta input file (genomic sequence):
	    
		>chr_17
		ccctaaccctaaccctaaccctaaccctaaccctaaccctaaccctaaccctaaccctaa
		TACGCGCGCGCCTAACCCTACGACTTTAACCTACTCTAAACTCTCCTACTAGTACGTCTT
    
	VCF input file :


		##fileformat=VCFv4.1
		##fileDate=20140725
		##source=freeBayes v0.9.13-2-ga830efd
		##reference=exmple.fsa
		##phasing=none
		##DetectedFormat=freebayes
		##FILTER=<ID=G_AN,Description="The SNP has been filtered ; out of AN range(over 2)">
		##FILTER=<ID=G_AF,Description="The SNP has been filtered ; out of AF range(under 0.9)">
		##FILTER=<ID=G_DP,Description="The SNP has been filtered ; out of DP range(15 - 35)">
		##FILTER=<ID=InDel,Description="The SNP has been filtered ; InDel detected">
		##FILTER=<ID=Nmatch,Description="The SNP has been filtered ; reference base detected : N">
		#CHROM    POS    ID    REF    ALT    QUAL    FILTER    INFO    FORMAT    V1
		chr_17    17    .    A    G    529.213    G_AF;G_DP    AB=0.583333;ABP=5.18177;AC=1;AF=0.5;AN=2;AO=21;CIGAR=1X;DP=36;DPB=36;DPRA=0;EPP=3.1137;EPPR=3.15506;GTI=0;LEN=1;MEANALT=1;MQM=60;MQMR=60;NS=1;NUMALT=1;ODDS=77.012;PAIRED=1;PAIREDR=1;PAO=0;PQA=0;PQR=0;PRO=0;QA=751;QR=535;RO=15;RPP=5.59539;RPPR=4.31318;RUN=1;SAF=11;SAP=3.1137;SAR=10;SRF=5;SRP=6.62942;SRR=10;TYPE=snp;technology.illumina=1;G_AN=2;G_AF=0.58;G_DP=36;G_Base=G    GT:DP:RO:QR:AO:QA:GL    0/1:36:15:535:21:751:-10,0,-10
		chr_17    37    .    C    G    1082.38    .    AB=0;ABP=0;AC=2;AF=1;AN=2;AO=34;CIGAR=1X;DP=34;DPB=34;DPRA=0;EPP=3.26577;EPPR=0;GTI=0;LEN=1;MEANALT=1;MQM=60;MQMR=0;NS=1;NUMALT=1;ODDS=48.0391;PAIRED=1;PAIREDR=0;PAO=0;PQA=0;PQR=0;PRO=0;QA=1243;QR=0;RO=0;RPP=15.5282;RPPR=0;RUN=1;SAF=18;SAP=3.26577;SAR=16;SRF=0;SRP=0;SRR=0;TYPE=snp;technology.illumina=1;G_AN=2;G_AF=1.00;G_DP=34;G_Base=G    GT:DP:RO:QR:AO:QA:GL    1/1:34:0:0:34:1243:-10,-9.23017,0
		chr_17    40    .    T    T    825.518    G_AF    AB=0;ABP=0;AC=2;AF=1;AN=2;AO=29;CIGAR=1X;DP=34;DPB=34;DPRA=0;EPP=6.67934;EPPR=13.8677;GTI=0;LEN=1;MEANALT=1;MQM=60;MQMR=60;NS=1;NUMALT=1;ODDS=8.92992;PAIRED=1;PAIREDR=1;PAO=0;PQA=0;PQR=0;PRO=0;QA=1082;QR=178;RO=5;RPP=9.07545;RPPR=13.8677;RUN=1;SAF=13;SAP=3.68421;SAR=16;SRF=5;SRP=13.8677;SRR=0;TYPE=snp;technology.illumina=1;G_AN=2;G_AF=0.85;G_DP=34;G_Base=T    GT:DP:RO:QR:AO:QA:GL    1/1:34:5:178:29:1082:-10,0,-6.82575
		chr_17    60    .    A    .    699.741    .    AB=0;ABP=0;AC=2;AF=1;AN=2;AO=22;CIGAR=1X;DP=22;DPB=22;DPRA=0;EPP=17.2236;EPPR=0;GTI=0;LEN=1;MEANALT=1;MQM=60;MQMR=0;NS=1;NUMALT=1;ODDS=32.2544;PAIRED=1;PAIREDR=0;PAO=0;PQA=0;PQR=0;PRO=0;QA=823;QR=0;RO=0;RPP=9.32731;RPPR=0;RUN=1;SAF=12;SAP=3.40511;SAR=10;SRF=0;SRP=0;SRR=0;TYPE=snp;technology.illumina=1;G_AN=2;G_AF=1.00;G_DP=22;G_Base=G    GT:DP:RO:QR:AO:QA:GL    1/1:22:0:0:22:823:-10,-5.98732,0
		chr_17    73    .    T    .    846.299    .    AB=0;ABP=0;AC=2;AF=1;AN=2;AO=27;CIGAR=1X;DP=27;DPB=27;DPRA=0;EPP=16.6021;EPPR=0;GTI=0;LEN=1;MEANALT=1;MQM=60;MQMR=0;NS=1;NUMALT=1;ODDS=38.84;PAIRED=1;PAIREDR=0;PAO=0;PQA=0;PQR=0;PRO=0;QA=1002;QR=0;RO=0;RPP=5.02092;RPPR=0;RUN=1;SAF=21;SAP=21.1059;SAR=6;SRF=0;SRP=0;SRR=0;TYPE=snp;technology.illumina=1;G_AN=2;G_AF=1.00;G_DP=27;G_Base=T    GT:DP:RO:QR:AO:QA:GL    1/1:27:0:0:27:1002:-10,-7.34226,0
		chr_17    81    .    C    T    764.464    .    AB=0;ABP=0;AC=2;AF=1;AN=2;AO=25;CIGAR=1X;DP=25;DPB=25;DPRA=0;EPP=13.5202;EPPR=0;GTI=0;LEN=1;MEANALT=1;MQM=60;MQMR=0;NS=1;NUMALT=1;ODDS=36.1324;PAIRED=1;PAIREDR=0;PAO=0;PQA=0;PQR=0;PRO=0;QA=902;QR=0;RO=0;RPP=3.79203;RPPR=0;RUN=1;SAF=19;SAP=17.6895;SAR=6;SRF=0;SRP=0;SRR=0;TYPE=snp;technology.illumina=1;G_AN=2;G_AF=1.00;G_DP=25;G_Base=T    GT:DP:RO:QR:AO:QA:GL    1/1:25:0:0:25:902:-10,-6.76842,0
		chr_17    105    .    C    T    1154    G_DP    AB=0;ABP=0;AC=2;AF=1;AN=2;AO=37;CIGAR=1X;DP=37;DPB=37;DPRA=0;EPP=5.88603;EPPR=0;GTI=0;LEN=1;MEANALT=1;MQM=60;MQMR=0;NS=1;NUMALT=1;ODDS=52.0047;PAIRED=1;PAIREDR=0;PAO=0;PQA=0;PQR=0;PRO=0;QA=1336;QR=0;RO=0;RPP=19.9713;RPPR=0;RUN=1;SAF=23;SAP=7.76406;SAR=14;SRF=0;SRP=0;SRR=0;TYPE=snp;technology.illumina=1;G_AN=2;G_AF=1.00;G_DP=37;G_Base=T    GT:DP:RO:QR:AO:QA:GL    1/1:37:0:0:37:1336:-10,-10,0
		chr_17    112    .    G    A    1276.25    G_DP    AB=0;ABP=0;AC=2;AF=1;AN=2;AO=40;CIGAR=1X;DP=40;DPB=40;DPRA=0;EPP=10.8276;EPPR=0;GTI=0;LEN=1;MEANALT=1;MQM=60;MQMR=0;NS=1;NUMALT=1;ODDS=55.9501;PAIRED=1;PAIREDR=0;PAO=0;PQA=0;PQR=0;PRO=0;QA=1471;QR=0;RO=0;RPP=10.8276;RPPR=0;RUN=1;SAF=26;SAP=10.8276;SAR=14;SRF=0;SRP=0;SRR=0;TYPE=snp;technology.illumina=1;G_AN=2;G_AF=1.00;G_DP=40;G_Base=A    GT:DP:RO:QR:AO:QA:GL    1/1:40:0:0:40:1471:-10,-10,0

	tab file with VCF infos : 
		
		V1	/my/path/VCF1.vcf

expected result :
-----------------

	CHROM    POS    reference    V1
	chr_17    1    C    U
	chr_17    2    C    U
	chr_17    3    C    U
	chr_17    4    T    U
	chr_17    5    A    U
	chr_17    6    A    U
	chr_17    7    C    U
	chr_17    8    C    U
	chr_17    9    C    U
	chr_17    10    T    U
	chr_17    11    A    U
	chr_17    12    A    U
	chr_17    13    C    U
	chr_17    14    C    U
	chr_17    15    C    U
	chr_17    16    T    U
	chr_17    17    A    F
	chr_17    18    A    U
	chr_17    19    C    U
	chr_17    20    C    U
	chr_17    21    C    U
	chr_17    22    T    U
	chr_17    23    A    U
	chr_17    24    A    U
	chr_17    25    C    U
	chr_17    26    C    U
	chr_17    27    C    U
	chr_17    28    T    U
	chr_17    29    A    U
	chr_17    30    A    U
	chr_17    31    C    U
	chr_17    32    C    U
	chr_17    33    C    U
	chr_17    34    T    U
	chr_17    35    A    U
	chr_17    36    A    U
	chr_17    37    C    G
	chr_17    38    C    U
	chr_17    39    C    U
	chr_17    40    T    F
	chr_17    41    A    U
	chr_17    42    A    U
	chr_17    43    C    U
	chr_17    44    C    U
	chr_17    45    C    U
	chr_17    46    T    U
	chr_17    47    A    U
	chr_17    48    A    U
	chr_17    49    C    U
	chr_17    50    C    U
	chr_17    51    C    U
	chr_17    52    T    U
	chr_17    53    A    U
	chr_17    54    A    U
	chr_17    55    C    U
	chr_17    56    C    U
	chr_17    57    C    U
	chr_17    58    T    U
	chr_17    59    A    U
	chr_17    60    A    R
	chr_17    61    T    U
	chr_17    62    A    U
	chr_17    63    C    U
	chr_17    64    G    U
	chr_17    65    C    U
	chr_17    66    G    U
	chr_17    67    C    U
	chr_17    68    G    U
	chr_17    69    C    U
	chr_17    70    G    U
	chr_17    71    C    U
	chr_17    72    C    U
	chr_17    73    T    R
	chr_17    74    A    U
	chr_17    75    A    U
	chr_17    76    C    U
	chr_17    77    C    U
	chr_17    78    C    U
	chr_17    79    T    U
	chr_17    80    A    U
	chr_17    81    C    T
	chr_17    82    G    U
	chr_17    83    A    U
	chr_17    84    C    U
	chr_17    85    T    U
	chr_17    86    T    U
	chr_17    87    T    U
	chr_17    88    A    U
	chr_17    89    A    U
	chr_17    90    C    U
	chr_17    91    C    U
	chr_17    92    T    U
	chr_17    93    A    U
	chr_17    94    C    U
	chr_17    95    T    U
	chr_17    96    C    U
	chr_17    97    T    U
	chr_17    98    A    U
	chr_17    99    A    U
	chr_17    100    A    U
	chr_17    101    C    U
	chr_17    102    T    U
	chr_17    103    C    U
	chr_17    104    T    U
	chr_17    105    C    F
	chr_17    106    C    U
	chr_17    107    T    U
	chr_17    108    A    U
	chr_17    109    C    U
	chr_17    110    T    U
	chr_17    111    A    U
	chr_17    112    G    F
	chr_17    113    T    U
	chr_17    114    A    U
	chr_17    115    C    U
	chr_17    116    G    U
	chr_17    117    T    U
	chr_17    118    C    U
	chr_17    119    T    U
	chr_17    120    T    U

	
------------------------------------------------------------
======================= VCFCarto.py ========================
------------------------------------------------------------

input :
-------

	CHROM	POS	reference	REF1	G01	REF2	G02	G03	G04	G05	G06	G07	G08	G09	G10	G11	G12
	Chr1	1	A	R	R	R	R	U	R	R	R	R	R	R	R	R	R
	Chr1	2	T	R	R	R	R	R	U	R	R	R	R	R	R	R	R
	Chr1	3	G	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr1	4	G	R	R	R	R	R	R	R	R	R	R	R	R	F	R
	Chr1	5	G	R	R	R	R	R	R	U	F	R	R	R	R	R	R
	Chr1	6	C	R	R	R	R	R	R	R	R	R	R	R	R	R	U
	Chr1	7	A	G	C	C	C	F	C	C	C	C	C	G	C	G	G
	Chr1	8	G	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr1	9	C	R	T	T	R	T	T	T	U	R	T	R	T	T	T
	Chr1	10	T	R	R	R	R	R	R	R	R	R	R	R	R	R	U
	Chr1	11	T	R	R	R	R	R	R	R	R	R	R	R	F	R	R
	Chr1	12	A	R	R	R	R	U	R	R	R	R	F	R	R	R	R
	Chr1	13	A	R	R	G	G	R	F	R	F	G	R	G	R	R	F
	Chr1	14	A	R	R	R	R	R	R	R	R	F	R	R	R	R	R
	Chr1	15	G	R	R	R	U	R	F	R	R	R	R	R	R	U	U
	Chr1	16	G	A	R	R	A	R	R	U	F	R	R	A	A	R	A
	Chr1	17	A	R	G	G	R	U	R	R	G	G	R	G	U	R	G
	Chr1	18	C	R	R	R	R	R	U	R	R	R	R	R	R	R	R
	Chr1	19	G	C	U	R	C	R	C	U	R	R	C	C	C	R	C
	Chr1	20	G	A	U	R	A	R	A	U	R	R	A	A	A	R	A
	Chr1	21	G	T	U	R	T	R	T	U	R	R	T	T	T	R	T
	Chr1	22	A	T	U	R	T	R	T	U	R	R	T	T	T	R	T
	Chr1	23	C	T	T	R	T	R	R	R	T	R	U	T	R	T	T
	Chr1	24	T	R	R	R	R	R	U	R	R	R	R	R	R	R	F
	Chr1	25	G	R	F	R	R	R	R	R	U	R	F	R	R	R	R
	Chr1	26	T	R	R	C	C	C	C	C	R	R	C	R	C	R	U
	Chr1	27	C	R	R	G	G	G	G	R	G	R	G	R	G	R	R
	Chr1	28	C	G	T	T	T	G	G	T	T	F	T	G	T	T	G
	Chr1	29	G	T	R	R	R	R	T	R	T	R	T	T	R	T	R
	Chr1	30	T	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr1	31	A	R	R	R	R	F	R	R	R	R	F	R	R	R	R
	Chr1	32	A	G	G	R	G	G	G	R	R	G	G	G	G	G	R
	Chr1	33	G	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr1	34	C	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr1	35	C	R	R	R	R	R	F	R	R	R	R	R	R	R	U
	Chr2	1	T	R	R	R	F	R	R	R	R	R	R	R	R	R	R
	Chr2	2	A	C	R	R	C	C	U	R	R	R	R	C	C	C	U
	Chr2	3	C	R	R	R	R	R	R	U	R	R	R	R	R	R	R
	Chr2	4	C	R	R	R	R	R	R	R	U	R	R	R	R	F	R
	Chr2	5	T	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr2	6	C	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr2	7	A	T	F	R	U	R	T	T	T	R	T	T	F	T	T
	Chr2	8	T	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr2	9	C	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr2	10	G	R	T	T	T	T	R	T	R	R	R	R	R	U	R
	Chr2	11	C	R	A	A	A	A	R	A	R	R	R	R	R	U	R
	Chr2	12	A	R	T	T	T	T	R	T	R	R	R	R	R	U	R
	Chr2	13	T	R	C	C	C	C	R	C	R	R	R	R	R	U	R
	Chr2	14	C	T	A	A	T	A	T	A	T	A	T	T	A	A	A
	Chr2	15	T	R	R	R	F	R	R	R	R	R	R	R	R	R	R
	Chr2	16	A	R	R	R	R	R	R	R	U	R	R	R	R	R	R
	Chr2	17	A	R	U	R	R	R	R	R	R	R	R	R	R	R	F
	Chr2	18	G	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr2	19	A	R	R	R	R	R	R	F	R	R	R	R	R	R	R
	Chr2	20	C	R	R	R	R	R	R	R	F	R	R	R	R	R	R
	Chr2	21	G	A	R	R	A	A	A	R	R	R	A	A	R	R	R
	Chr2	22	A	R	R	R	R	R	R	F	R	R	R	R	R	R	R
	Chr2	23	A	R	R	T	T	R	R	T	T	T	T	T	R	R	R
	Chr2	24	T	R	R	R	R	R	R	U	R	R	R	R	R	R	F
	Chr2	25	T	R	A	A	R	R	A	R	A	R	R	A	R	R	A
	Chr2	26	G	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr2	27	A	R	R	R	R	R	R	R	R	R	R	R	R	U	R
	Chr2	28	C	R	U	R	R	F	F	R	R	F	R	F	U	R	R
	Chr2	29	G	R	R	R	R	R	R	F	R	R	R	R	R	R	R
	Chr2	30	T	A	A	G	A	G	G	A	A	G	F	G	G	G	U
	Chr2	31	A	R	R	R	R	R	R	R	R	U	U	R	R	R	R
	Chr2	32	G	R	R	R	R	R	R	U	U	R	R	R	R	R	R
	Chr2	33	G	R	U	R	R	R	R	U	R	R	R	R	R	R	R
	Chr2	34	A	R	R	R	U	R	R	R	R	R	R	R	R	R	R
	Chr2	35	G	R	R	R	R	R	R	R	R	R	R	R	R	R	R
	Chr2	36	T	R	R	R	R	R	R	U	R	R	R	R	R	R	R
	Chr3	1	T	U	R	R	R	R	R	U	R	R	R	R	R	R	R
	Chr3	2	T	R	R	U	R	R	R	U	R	R	R	R	R	R	R
	Chr3	3	T	F	R	R	R	R	R	U	R	R	R	R	R	R	R
	Chr3	4	T	R	R	F	R	R	R	U	R	R	R	R	R	R	R


output :
--------

- without A - H code : 

	CHROM	POS	reference	REF1	G01	REF2	G02	G03	G04	G05	G06	G07	G08	G09	G10	G11	G12
	Chr1	7	A	G	C	C	C	F	C	C	C	C	C	G	C	G	G
	Chr1	9	C	R	T	T	R	T	T	T	U	R	T	R	T	T	T
	Chr1	13	A	R	R	G	G	R	F	R	F	G	R	G	R	R	F
	Chr1	16	G	A	R	R	A	R	R	U	F	R	R	A	A	R	A
	Chr1	17	A	R	G	G	R	U	R	R	G	G	R	G	U	R	G
	Chr1	19	G	C	U	R	C	R	C	U	R	R	C	C	C	R	C
	Chr1	20	G	A	U	R	A	R	A	U	R	R	A	A	A	R	A
	Chr1	21	G	T	U	R	T	R	T	U	R	R	T	T	T	R	T
	Chr1	22	A	T	U	R	T	R	T	U	R	R	T	T	T	R	T
	Chr1	23	C	T	T	R	T	R	R	R	T	R	U	T	R	T	T
	Chr1	26	T	R	R	C	C	C	C	C	R	R	C	R	C	R	U
	Chr1	27	C	R	R	G	G	G	G	R	G	R	G	R	G	R	R
	Chr1	28	C	G	T	T	T	G	G	T	T	F	T	G	T	T	G
	Chr1	29	G	T	R	R	R	R	T	R	T	R	T	T	R	T	R
	Chr1	32	A	G	G	R	G	G	G	R	R	G	G	G	G	G	R
	Chr2	2	A	C	R	R	C	C	U	R	R	R	R	C	C	C	U
	Chr2	7	A	T	F	R	U	R	T	T	T	R	T	T	F	T	T
	Chr2	10	G	R	T	T	T	T	R	T	R	R	R	R	R	U	R
	Chr2	11	C	R	A	A	A	A	R	A	R	R	R	R	R	U	R
	Chr2	12	A	R	T	T	T	T	R	T	R	R	R	R	R	U	R
	Chr2	13	T	R	C	C	C	C	R	C	R	R	R	R	R	U	R
	Chr2	14	C	T	A	A	T	A	T	A	T	A	T	T	A	A	A
	Chr2	21	G	A	R	R	A	A	A	R	R	R	A	A	R	R	R
	Chr2	23	A	R	R	T	T	R	R	T	T	T	T	T	R	R	R
	Chr2	25	T	R	A	A	R	R	A	R	A	R	R	A	R	R	A
	Chr2	30	T	A	A	G	A	G	G	A	A	G	F	G	G	G	U

- with A - H code but no markers :

	CHROM	POS	reference	REF1	G01	REF2	G02	G03	G04	G05	G06	G07	G08	G09	G10	G11	G12
	Chr1	7	-	A	H	H	H	-	H	H	H	H	H	A	H	A	A
	Chr1	9	-	A	H	H	A	H	H	H	-	A	H	A	H	H	H
	Chr1	13	-	A	A	H	H	A	-	A	-	H	A	H	A	A	-
	Chr1	16	-	A	H	H	A	H	H	-	-	H	H	A	A	H	A
	Chr1	17	-	A	H	H	A	-	A	A	H	H	A	H	-	A	H
	Chr1	19	-	A	-	H	A	H	A	-	H	H	A	A	A	H	A
	Chr1	20	-	A	-	H	A	H	A	-	H	H	A	A	A	H	A
	Chr1	21	-	A	-	H	A	H	A	-	H	H	A	A	A	H	A
	Chr1	22	-	A	-	H	A	H	A	-	H	H	A	A	A	H	A
	Chr1	23	-	A	A	H	A	H	H	H	A	H	-	A	H	A	A
	Chr1	26	-	A	A	H	H	H	H	H	A	A	H	A	H	A	-
	Chr1	27	-	A	A	H	H	H	H	A	H	A	H	A	H	A	A
	Chr1	28	-	A	H	H	H	A	A	H	H	-	H	A	H	H	A
	Chr1	29	-	A	H	H	H	H	A	H	A	H	A	A	H	A	H
	Chr1	32	-	A	A	H	A	A	A	H	H	A	A	A	A	A	H
	Chr2	2	-	A	H	H	A	A	-	H	H	H	H	A	A	A	-
	Chr2	7	-	A	-	H	-	H	A	A	A	H	A	A	-	A	A
	Chr2	10	-	A	H	H	H	H	A	H	A	A	A	A	A	-	A
	Chr2	11	-	A	H	H	H	H	A	H	A	A	A	A	A	-	A
	Chr2	12	-	A	H	H	H	H	A	H	A	A	A	A	A	-	A
	Chr2	13	-	A	H	H	H	H	A	H	A	A	A	A	A	-	A
	Chr2	14	-	A	H	H	A	H	A	H	A	H	A	A	H	H	H
	Chr2	21	-	A	H	H	A	A	A	H	H	H	A	A	H	H	H
	Chr2	23	-	A	A	H	H	A	A	H	H	H	H	H	A	A	A
	Chr2	25	-	A	H	H	A	A	H	A	H	A	A	H	A	A	H
	Chr2	30	-	A	A	H	A	H	H	A	A	H	-	H	H	H	-

- with A - H code but & markers  : 
 
 - tab file : 
 
	CHROM	POS	reference	REF1	G01	REF2	G02	G03	G04	G05	G06	G07	G08	G09	G10	G11	G12
	Chr1	*M_00001	-	A	H	H	H	-	H	H	H	H	H	A	H	A	A
	Chr1	*M_00002	-	A	H	H	A	H	H	H	-	A	H	A	H	H	H
	Chr1	*M_00003	-	A	A	H	H	A	-	A	-	H	A	H	A	A	-
	Chr1	*M_00004	-	A	H	H	A	H	H	-	-	H	H	A	A	H	A
	Chr1	*M_00005	-	A	H	H	A	-	A	A	H	H	A	H	-	A	H
	Chr1	*M_00006	-	A	-	H	A	H	A	-	H	H	A	A	A	H	A
	Chr1	*M_00007	-	A	A	H	A	H	H	H	A	H	-	A	H	A	A
	Chr1	*M_00008	-	A	A	H	H	H	H	H	A	A	H	A	H	A	-
	Chr1	*M_00009	-	A	A	H	H	H	H	A	H	A	H	A	H	A	A
	Chr1	*M_00010	-	A	H	H	H	A	A	H	H	-	H	A	H	H	A
	Chr1	*M_00011	-	A	H	H	H	H	A	H	A	H	A	A	H	A	H
	Chr1	*M_00012	-	A	A	H	A	A	A	H	H	A	A	A	A	A	H
	Chr2	*M_00013	-	A	H	H	A	A	-	H	H	H	H	A	A	A	-
	Chr2	*M_00014	-	A	-	H	-	H	A	A	A	H	A	A	-	A	A
	Chr2	*M_00015	-	A	H	H	H	H	A	H	A	A	A	A	A	-	A
	Chr2	*M_00016	-	A	H	H	A	H	A	H	A	H	A	A	H	H	H
	Chr2	*M_00017	-	A	H	H	A	A	A	H	H	H	A	A	H	H	H
	Chr2	*M_00018	-	A	A	H	H	A	A	H	H	H	H	H	A	A	A
	Chr2	*M_00019	-	A	H	H	A	A	H	A	H	A	A	H	A	A	H
	Chr2	*M_00020	-	A	A	H	A	H	H	A	A	H	-	H	H	H	-

 - bed file : 
    
	Chr1	7	7	*M_00001
	Chr1	9	9	*M_00002
	Chr1	13	13	*M_00003
	Chr1	16	16	*M_00004
	Chr1	17	17	*M_00005
	Chr1	19	22	*M_00006
	Chr1	23	23	*M_00007
	Chr1	26	26	*M_00008
	Chr1	27	27	*M_00009
	Chr1	28	28	*M_00010
	Chr1	29	29	*M_00011
	Chr1	32	32	*M_00012
	Chr2	2	2	*M_00013
	Chr2	7	7	*M_00014
	Chr2	10	13	*M_00015
	Chr2	14	14	*M_00016
	Chr2	21	21	*M_00017
	Chr2	23	23	*M_00018
	Chr2	25	25	*M_00019
	Chr2	30	30	*M_00020