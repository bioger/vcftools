#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from commons.core.LoggerFactory             import LoggerFactory
from commons.core.utils.RepetOptionParser   import RepetOptionParser
from commons.core.checker.RepetException    import RepetException
from commons.core.utils.FileUtils           import FileUtils
import os

LOG_NAME = "gnome_tools"

class VCFCarto(object):
    
    def __init__(self, tableName = "", verbosity = 3, outfile = "", ignoreParental=False, refA="", refB="",mergeMarker=False, onlyPar=False,stats=False, graphs = False):
        self._VCFFileName = tableName
        self._verbosity = verbosity
        self.setMergeMarker(mergeMarker)
        self.setOUTFile(outfile)
        self.setIgnoreParental(ignoreParental)
        self._refA = refA
        self._refH = refB
        self._onlyPar = onlyPar
        self._stats = stats
        self.setDoGraphs(graphs)
        self._log = LoggerFactory.createLogger("%s.%s" % (LOG_NAME, self.__class__.__name__), self._verbosity)
        
            
    def setAttributesFromCmdLine(self):
        self._toolVersion = "1.2"
        description = "VCFcarto version %s" % self._toolVersion
        epilog = "\n VCFcarto uses VCFStorage output as input. \n"
        epilog += "VCFcarto can convert your tabulated file into a file with only the SNP from refA and refH.\n"
        epilog += "2 formats are possible, either the input format is conserved, or the format is changed into a 3 letter format\n"
        epilog += "(\"A\" for refA, \"H\" for refH and \"-\" when the base do not correspond to any parent)\n\n"
        epilog += "example 1 : VCFcarto.py -f Storage.out -A G15 -H G23 -o FilteredStorage.out\n"
        epilog += "example 2 : VCFcarto.py -f Storage.out -A ref1 -H ref2 -p -s -g -m -o cartoTable.out\n"
        parser = RepetOptionParser(description = description, epilog = epilog, version = self._toolVersion) 
        parser.add_option("-f", "--file",      dest = "tableName", action = "store", type = "string", help = "Input TSV File name [compulsory] [format: TSV]",                                                                                                    default = "")
        parser.add_option("-o", "--output",    dest = "outFile",   action = "store", type = "string", help = "output TSV File name [compulsory] [format: TSV]",                                                                                                   default = "")
        parser.add_option("-i", "--ignoreParental",      dest = "ignoreParental",      action = "store_true", help = "ignore parental information. This option will overwrite -A -H -m -p",                                                                       default = False)
        parser.add_option("-A", "--refA",      dest = "refA",      action = "store", type = "string", help = "name of the reference genome A [compulsory] ",                                                                                                      default = "")
        parser.add_option("-H", "--refH",      dest = "refH",      action = "store", type = "string", help = "name of the reference genome H [compulsory] ",                                                                                                      default = "")
        parser.add_option("--refHCode",      dest = "refHCode",      action = "store", type = "string", help = "code used for the reference genome H, default=H ",default = "H")
        parser.add_option("--refACode",      dest = "refACode",      action = "store", type = "string", help = "code used for the reference genome A, default=A ",default = "A")
        parser.add_option("--refNACode",      dest = "refNACode",      action = "store", type = "string", help = "code used for no reference genome , default=- ",default = "-")
        parser.add_option("-p", "--onlyParents",   dest = "onlyPar",   action = "store_true",         help = "Will change every letters by either A or H depending on which parents the strain correspond to for that base[optional] [default: False]",           default = False)
        parser.add_option("-m", "--mergeMarkers",   dest = "mergeMarkers",   action = "store_true",   help = "Will merge sequential markers with the same information ; option -p is needed [optional] [default: False]",                                         default = False)
        parser.add_option("-s", "--statistics",dest = "stats",     action = "store_true",             help = "Gives statistics on stdout[optional] [default: False]",                                                                                             default = False)
        parser.add_option("-g", "--graphics",  dest = "graphs",    action = "store_true",             help = "create graphs. Only works with -p[optional] [default: False]",                                                                                      default = False)
        parser.add_option("-v", "--verbosity", dest = "verbosity", action = "store", type = "int",    help = "Verbosity [optional] [default: 3]",                                                                                                                 default = 3)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self.setTableName(options.tableName)
        self.setMergeMarker(options.mergeMarkers)
        self.setIgnoreParental(options.ignoreParental)
        self.setOUTFile(options.outFile)
        self.setRefA(options.refA)
        self.setRefH(options.refH)
        self.refACode = options.refACode
        self.refHCode = options.refHCode
        self.refNACode = options.refNACode
        self.setOnlyPar(options.onlyPar)
        self.setStats(options.stats)
        self.setDoGraphs(options.graphs)
        self.setVerbosity(options.verbosity)
            
    def setTableName(self, tableName):
        self._VCFFileName = tableName
        
    def setMergeMarker(self, mergeMarker) :
        self._mergeMarkers = mergeMarker
        
    def setIgnoreParental(self, ignoreParental):
        self._ignoreParental = ignoreParental
        
    def setOUTFile(self, outFile):
        if self._mergeMarkers :
            if  self._ignoreParental :
                self._outFile = outFile
                self._outMergedFile = ""
            else :
                self._outFile = "tmpVCFCarto.tmp"
                self._outMergedFile = outFile
        else :
            self._outFile = outFile
            self._outMergedFile = ""

    def setRefA(self, refA):
        self._refA = refA

    def setRefH(self, refH):
        self._refH = refH

    def setOnlyPar(self, onlyPar):
        self._onlyPar = onlyPar
        
    def setStats(self, stats):
        self._stats = stats
        
    def setDoGraphs(self, doGraphs):
        self._doGraphs = doGraphs
        self._graphsFolder = os.path.join(os.getcwd(), "VCFCarto_graphs")
        
    def setVerbosity(self, verbosity):
        self._verbosity = verbosity
        
    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise RepetException(errorMsg)

    def checkoption(self):
        if self._outFile == "":
            self._logAndRaise("Missing output file destination")
        else:
            if FileUtils.isRessourceExists(self._outFile):
#                self._logAndRaise("Output file '%s' already exists!" % self._outFile)
                pass
            
        if self._VCFFileName == "":
            self._logAndRaise("Missing input file")
        else:
            if FileUtils.isRessourceExists(self._VCFFileName):
                self._VCFFileName = os.path.abspath(self._VCFFileName)
            else:
                self._logAndRaise("Input file '%s' does not exists!" % self._VCFFileName)
                
        if self._ignoreParental is True :
            self._log.info("option set to -i ; ignoring options -m -p -g ")
        else : 

            if self._mergeMarkers :
                if self._onlyPar:
                    if self._outMergedFile == "" :
                        self._logAndRaise("Missing output file destination")
                    else : 
                        if FileUtils.isRessourceExists(self._outMergedFile):
                            pass
    #                        self._logAndRaise("Output file '%s' already exists!" % self._outMergedFile)
                else :
                    self._logAndRaise("option -m needs option -p")
    
            if self._doGraphs :
                if self._onlyPar :
                    if FileUtils.isRessourceExists(self._graphsFolder):
                        self._log.error("folder %s already exists, will skip graph creation" % self._graphsFolder)
                    else :
                        self._log.info("create graphs on folder %s" % self._graphsFolder)
                        os.mkdir(self._graphsFolder)
                    
                else : 
                    self._log.error("can't create graphs without option -r, will skip graph creation")
                    self._doGraphs = False                
                
    def VCF2Carto(self):
        if self._stats :
            self._statsGenomes = []
        with open(self._VCFFileName, "r") as f:
            line = f.readline()
            (coordRefA, coordRefB) = self.findParentalGenomes(line)
            output = open(self._outFile, "w")
            output.write(line)
            line = f.readline()
            if self._stats : 
                print("%s %s %s %s %s" % ("CHROM", "position", "nbRefA", "nbRefB", "others"))
            while line :
                (diff,line) = self.parseLine(line,coordRefA, coordRefB)
                if diff : 
                    output.write(line)
                line = f.readline()
                
    def findParentalGenomes(self,line):
        coordRefA = 0
        coordRefB = 0
        lGNames = line.rstrip().split("\t")
        for index,gName in enumerate(lGNames):
            if self._stats :
                self._statsGenomes.append([gName, 0, 0, 0])
            if gName == self._refA :
                if coordRefA == 0 : 
                    coordRefA = index
                else :
                    self._logAndRaise("refA has been found twice")
            if gName == self._refH :
                if coordRefB == 0 : 
                    coordRefB = index
                else :
                    self._logAndRaise("refB has been found twice")
        
        if coordRefA == 0 :
            self._logAndRaise("refA hasn't been found")
        if coordRefB == 0 : 
            self._logAndRaise("refB hasn't been found")
        
        return (coordRefA,coordRefB)
        
    def parseLine(self,line,coordRefA, coordRefB):
        diff = False
        lGBases = line.rstrip().split("\t")
        
        if lGBases[coordRefA] != lGBases[coordRefB] and lGBases[coordRefA] != "U" and lGBases[coordRefA] != "F" and lGBases[coordRefB] != "U" and lGBases[coordRefB] != "F" :
            diff = True
            if self._onlyPar or self._stats : 
                if self._stats :
                    statsSNP = [0,0,0,0]
                
                RefA = lGBases[coordRefA]
                RefB = lGBases[coordRefB]
                for index,gBase in enumerate(lGBases):
                    if index >1 : 
                        if gBase == RefA:
                            if self._stats : 
                                self._statsGenomes[index][1] += 1
                                statsSNP[0] +=1
                            if self._onlyPar :
                                lGBases[index] = self.refACode 
                        elif gBase == RefB:
                            if self._stats : 
                                self._statsGenomes[index][2] += 1
                                statsSNP[1] +=1
                            if self._onlyPar :
                                lGBases[index] = self.refHCode
                        else :
                            if self._stats : 
                                self._statsGenomes[index][3] += 1
                                statsSNP[2] +=1
                            if self._onlyPar :
                                lGBases[index] = self.refNACode
                line="\t".join(lGBases)
                line="%s\n" %line
                if self._stats : 
                    print("%s %s %s %s %s" % (lGBases[0], lGBases[1], statsSNP[0], statsSNP[1], statsSNP[2]))
        return diff,line
        
    def GenomeStats(self):
        if  self._stats : 
            print("\n\n")
            for index,l in enumerate(self._statsGenomes) :
                if index > 1 :
                    print("%s %d %d %d" % (l[0],l[1],l[2],l[3])) 
        
    def reduceCartoSize(self):
        if self._mergeMarkers :
            with open(self._outFile, "r") as f:
                line = f.readline()
                output = open(self._outMergedFile, "w")
                output.write(line)
                line = f.readline()
                
                markerlist = "markerList.bed"
                print("%s" %(os.getcwd()))
                bedfile = open(markerlist, "w")
                markerNum = 0
                markerName = "*M_%05d" %markerNum
                chrom=""
                start=""
                end=""
                seq=""
                
                while line :
                    (chrLine,posLine,refLine,seqLine) = line.rstrip().split("\t", 3)
                    if chrom and chrLine == chrom and seqLine==seq :
                        end = posLine
                    else :
                        if chrom :
                            markerNum +=1
                            markerName = "*M_%05d" %markerNum
                            bedfile.write("%s\t%s\t%s\t%s\n" % (chrom, start, end, markerName))
                            output.write("%s\t%s\t-\t%s\n" %(chrom, markerName,seq))
                        else :
                            for base in seqLine.split("\t") :
                                if base != self.refACode and base != self.refHCode and base != self.refNACode :
                                    self._logAndRaise("Should use the cartographic matrix from VCFCarto, not the generic one (use -r option on VCFCarto)\n Not recognized base %s, only A H - should be used." % base)
                        (chrom,start,end,seq) = (chrLine,posLine,posLine,seqLine)
                    line = f.readline()
                markerNum +=1
                markerName = "*M_%05d" %markerNum
                bedfile.write("%s\t%s\t%s\t%s\n" % (chrom, start, end, markerName))
                output.write("%s\t%s\t-\t%s\n" %(chrom, markerName,seq))
                (chrom,start,end,seq) = (chrLine,posLine,posLine,seqLine)
            os.remove(self._outFile)
        
    def generatetemporaryfileforRgraph(self):
        dChrom = {}
        if self._mergeMarkers :
            self._log.info("start generating graphs for file %s" %self._outMergedFile)
            f = open(self._outMergedFile, "r")
        else :
            self._log.info("start generating graphs for file %s" %self._outFile)
            f = open(self._outFile, "r")
        line = f.readline()
        (chromtitle,postitle,refTitle,genomelist) = line.rstrip().split("\t",3)
        lgenomes = genomelist.rstrip().split("\t")
        genomeNum = len(lgenomes)
        
        tmpOut = "tmpTableHmap.tab"
        self._tmpOut = os.path.join(self._graphsFolder, tmpOut)
        
        output = open(self._tmpOut, "w")
        line = "Name\t%s\n" % ("\t".join(lgenomes))
        output.write(line)
        line = f.readline()
        
        (increm,startPos,lastCHROM) = (0,0,"none")
        while line :
            increm+=1
            (CHROM,POS,ref,tmp) = line.rstrip().split("\t", 3)
            
            if lastCHROM == "none" : 
                lastCHROM = CHROM
                startPos = increm
            elif lastCHROM != CHROM : 
                dChrom[lastCHROM]= (startPos, increm-1)
                lastCHROM = CHROM
                startPos = increm
            lValues = tmp.rstrip().split("\t")
            for i,val in enumerate(lValues) : 
                if val == self.refHCode :
                    lValues[i] = "1"
                elif val == self.refACode : 
                    lValues[i] = "-1" 
                elif val == self.refNACode : 
                    lValues[i] = "0"
                else : 
                    self._logAndRaise("can't manage value %s ; only A H and - allowed" % val)
            line = "\t".join(("_".join((CHROM,POS)),"\t".join(lValues)))
            output.write(line)
            output.write("\n")
            line = f.readline()
        dChrom[lastCHROM]= (startPos, increm)
        return(dChrom, genomeNum)
    
    
    
    def createRscript(self, dChrom, genomeNum):
        self._graphFormat="png"
        scriptFileName = "tmpScript.R"
        self._scriptFileName = os.path.join(self._graphsFolder, scriptFileName)
        output = open(self._scriptFileName, "w")
        rScript = "library(lattice)\n"
        rScript +="tab <- read.table(\"%s\", head = TRUE , row = \"Name\")\n" %(self._tmpOut)
        rScript +="tab2 <- as.matrix(tab)\n"
        
        for chrom in dChrom :
            (start, stop) = dChrom[chrom]
            if start == stop :
                self._log.info("skip chrom %s (not enough markers)" %chrom)
            else : 
                self._log.debug("working on %s...\n" %chrom)
                width = min(max((stop-start)*9 + 100, 34*(13 + len(chrom))),32700)
                height = max(genomeNum*9 + 200, 400)
                labelSize = min (2.5, genomeNum*0.1)
                rScript +="%s(\"%s/%s.%s\", width = %d, height = %d)\n" %(self._graphFormat,self._graphsFolder, chrom,self._graphFormat, width, height)
                rScript += "levelplot(tab2[%d:%d,], col.regions = c(\"blue\",\"white\",\"red\"), cut = 2, scales=list(x=list(rot=90)), main=list(label=\"markers of %s\",cex=2), xlab =list(label=\"markers\",cex=%s) , ylab =list(label=\"strain\",cex=2.5) , colorkey=list(labels=list(at=c(-0.75,0, 0.75),labels=c(\"%s\", \"?\", \"%s\"),cex=1),height=0.2,width=5))\n"% (start, stop, chrom,labelSize,self._refA ,self._refH )
                rScript += "graphics.off()\n\n"
            
        output.write(rScript)
        output.close()
        
    def launchRscript(self):
        cmd ="Rscript %s" %(self._scriptFileName)
        self._log.info("generating graphs...")
        self._log.debug("start command %s"%cmd)
        os.system(cmd)
        
    def clean(self):
        self._log.info("remove temporary files...")
        os.remove(self._tmpOut)
        os.remove(self._scriptFileName)
        
        
    def cartoWithoutParents(self):
        with open(self._VCFFileName, "r") as f:
            line = f.readline()
            output = open(self._outFile, "w")
            output.write(line)
            line = f.readline()
            while line :
                (diff,line) = self.reduceCartoSizeNoParents(line)
                if diff : 
                    output.write(line)
                line = f.readline()
        output.close()
                       
    def reduceCartoSizeNoParents(self,line):
        diff = False
        lLinesplit = line.rstrip().split("\t", 3)
        lGBases = lLinesplit[3].split("\t")
        dBases = {}
        for base in lGBases : 
            dBases[base] = dBases.get(base, 0) + 1
        if "R" in dBases.keys():
            tot = dBases["R"]
            for SNP in ("A","T","G","C"):
                if SNP in dBases.keys():
                    tot += dBases[SNP]
                    val = float(dBases[SNP])/(float(dBases["R"])+dBases[SNP])
                    if val > 0.05 and val < 0.95 : 
                        diff = True
#            if tot < 30 :
#                diff = False
        return diff,line
        
    def run(self):
        self.checkoption()
        if self._ignoreParental is True :
            self.cartoWithoutParents()
        else : 
            self.VCF2Carto()
            self.reduceCartoSize()
            self.GenomeStats()
            if self._doGraphs :
                dChrom, genomeNum = self.generatetemporaryfileforRgraph()
                self.createRscript(dChrom, genomeNum)
                self.launchRscript()
                self.clean()
        

if __name__== "__main__":
    iVCFCarto = VCFCarto()
    iVCFCarto.setAttributesFromCmdLine()
    iVCFCarto.run()


