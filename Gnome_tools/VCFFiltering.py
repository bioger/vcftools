#!/usr/bin/env python


# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.


from commons.core.LoggerFactory             import LoggerFactory
from commons.core.utils.RepetOptionParser   import RepetOptionParser
from commons.core.checker.RepetException    import RepetException
from commons.core.utils.FileUtils           import FileUtils
import os
import re
import sqlite3

LOG_NAME = "gnome_tools"


class VCFFiltering(object):

    def __init__(self, VCFFileName = "", verbosity = 7, outile = "", minDP = 0, maxDP = 0, AN = 2, AF = 0.9, qual = 1, lBedFiles = [], stats=False, graphs=False, graphicFormat="pdf"):
        self._VCFFileName = VCFFileName
        self._verbosity = verbosity
        self._VCFFSource = ""
        self._outFile = outile
        self._minDP = minDP
        self._maxDP = maxDP
        self._AN = AN
        self._AF = AF
        self._qual = qual
        self._lBedFiles = lBedFiles
        self._stats = stats
        self.setDoGraphs(graphs)
        self.setGraphFormat(graphicFormat)
        self._graphsFolder = "VCFFiltering_graphs"
        self.db = 'test.db'
        self._log = LoggerFactory.createLogger("%s.%s" % (LOG_NAME, self.__class__.__name__), self._verbosity)



    def setAttributesFromCmdLine(self):
        self._toolVersion = "1.3"
        description = "VCFFiltering version %s" % self._toolVersion
        epilog = "\nFilters SNP on a VCF depending on depth (DP) allele number (AN), allele frequency (AF) and SNP quality.\n"
        epilog += "VCFFitlering can filter VCF from Freebayes, Mpileup and NaiveVariantCaller\n"
        epilog += "example 1 : VCFFiltering.py -f myVCF.vcf -o FilteredVCF.vcf\n"
        epilog += "example 2 : VCFFiltering.py -f myVCF.vcf -N 2 -F 0.87 -b bed1.bed -b bed2.bed -o FilteredVCF.vcf\n"
        parser = RepetOptionParser(description = description, epilog = epilog, version = self._toolVersion)
        parser.add_option("-f", "--vcf",       dest = "VCFFile",   action = "store", type = "string", help = "Input VCF File name [compulsory] [format: VCF]",                                            default = "")
        parser.add_option("-o", "--output",    dest = "outFile",   action = "store", type = "string", help = "output VCF File name [compulsory] [format: VCF]",                                           default = "")
        parser.add_option("-m", "--minDP",     dest = "minDP",     action = "store", type = "int",    help = "minimum of depth ; if both minDP and maxDP are set, optimal DP will not be calculated ",    default = 0)
        parser.add_option("-M", "--maxDP",     dest = "maxDP",     action = "store", type = "int",    help = "maximum of depth ; if both minDP and maxDP are set, optimal DP will not be calculated ",    default = 0)
        parser.add_option("-N", "--AN",        dest = "AN",        action = "store", type = "int",    help = "maximum number of allele for a SNP; default = 2",                                           default = 2)
        parser.add_option("-F", "--AF",        dest = "AF",        action = "store", type = "float",  help = "minimum frequency for the alternative allele of a SNP; default = 0.9",                      default = 0.9)
        parser.add_option("-Q", "--qual",      dest = "qual",      action = "store", type = "int",    help = "minimum quality for a SNP",                                                                 default = 1)
        parser.add_option("-b", "--bed",       dest = "bedFiles",  action = "append", type = "string", help = "bed files: list of coordinates to filter, multiple arguments allowed '-b file1 -b file2' ", default = [])
        parser.add_option("-g", "--graphics",  dest = "graphs",    action = "store_true",             help = "create graphs. Only works with -p[optional] [default: False]",                               default = False)
        parser.add_option("-G", "--graphicFormat",    dest = "graphicFormat",   action = "store", type = "string", help = "output format for the graph (pdf or png) [optional] [default: pdf]",                                           default = "pdf")
        parser.add_option("-v", "--verbosity", dest = "verbosity", action = "store", type = "int",    help = "Verbosity [optional] [default: 3]",                                                         default = 3)
        parser.add_option("-s", "--stats",     dest = "stats",     action = "store_true",             help = "output data for stats on DP [optional][default: False]",                                    default = False)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)


    def _setAttributesFromOptions(self, options):
        self.setVCFFileName(options.VCFFile)
        self.setOUTFile(options.outFile)
        self.setminDP(options.minDP)
        self.setmaxDP(options.maxDP)
        self.setAN(options.AN)
        self.setAF(options.AF)
        self.setqual(options.qual)
        self.setBEDFiles(options.bedFiles)
        self.setVerbosity(options.verbosity)
        self.setStats(options.stats)
        self.setDoGraphs(options.graphs)
        self.setGraphFormat(options.graphicFormat)
            
    def setVCFFileName(self, VCFFileName):
        self._VCFFileName = VCFFileName
        
    def setDoGraphs(self, doGraphs):
        self._doGraphs = doGraphs
        self._graphsFolder = os.path.join(os.getcwd(), "VCFFiltering_graphs")
        
    def setGraphFormat(self, graphFormat):
        if graphFormat.lower()=="png":
            self._graphFormat= "png"
        elif graphFormat.lower()=="pdf":
            self._graphFormat= "pdf"
        else :
            self._graphFormat= "pdf"
            self._log.info("graph format set to pdf")
        
    def setOUTFile(self, outFile):
        self._outFile = outFile

    def setminDP(self, minDP):
        self._minDP = int(minDP)

    def setmaxDP(self, maxDP):
        self._maxDP = int(maxDP)

    def setAN(self, AN):
        self._AN = int(AN)

    def setAF(self, AF):
        self._AF = float(AF)

    def setqual(self, qual):
        self._qual = int(qual)
        
    def setStats(self, stats):
        self._stats = stats
        
    def setVerbosity(self, verbosity):
        self._verbosity = verbosity

    def setBEDFiles(self, bedFiles):
        if bedFiles :
            if bedFiles == "":
                pass
            else :
                self._lBedFiles = bedFiles
                
                
    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise RepetException(errorMsg)

    def checkoption(self):
        if self._outFile == "":
            self._logAndRaise("Missing output VCF file destination")
        else:
            if FileUtils.isRessourceExists(self._outFile):
#                self._logAndRaise("Output VCF file '%s' already exist!" % self._outFile)
                pass

        if self._VCFFileName == "":
            self._logAndRaise("Missing input VCF file")
        else:
            if FileUtils.isRessourceExists(self._VCFFileName):
                self._VCFFileName = os.path.abspath(self._VCFFileName)
            else:
                self._logAndRaise("Input VCF file '%s' does not exist!" % self._VCFFileName)

    def medianFromDic(self, dic):
        total = 0

        if dic.keys() == []:
            self._logAndRaise("dictionary is empty")
        else :

            for val in dic.values():
                total += val
        median = 0
        cumul = 0
        i = 0
        while median == 0 :
            i +=1
            if i in dic:
                cumul += dic[i]
                if cumul >= total/2.0 :
                    median = i
        return median

    def SearchLocalMax(self, dic, approx):
        mValue = 0
        mKey = 0

        for i in range(int(max(3,approx/2.2)),int(approx*1.5)):
            if i in dic:
                if dic[i] > mValue:
                    mKey = i
                    mValue = dic[i]
            else :
                break
        return mKey

    def calculEcartType(self, Dic, DP):
        ecartType = 0
        for i in range(DP,len(Dic.keys())):
            if Dic[i]<Dic[DP]/2.0 :
                ecartType = int(((i-DP)/1.177)+1)
                self._log.debug("ecart type = %s" % ecartType)
                break
        return ecartType

    def getSource(self):
        with open(self._VCFFileName, "r") as f:
            m = re.search('^#.*',f.readline())
            while(m):
                if re.match('^##source=freeBayes.*',m.group(0)):
                    self._VCFFSource = "freebayes"
                elif re.match('^##samtoolsVersion=.*',m.group(0)):
                    self._VCFFSource = "mpileup"
                elif re.match('^##source=Dan.*',m.group(0)):
                    self._VCFFSource = "nvc"
                m = re.search('^#.*',f.readline())
            if self._VCFFSource == "" :
                self._logAndRaise("unknown VCF source (supported softwares : Freebayes, MPileup, Naive VaraintCaller")
        f.close()
        return self._VCFFSource

    def analyseDPDistribution(self):

        if self._minDP == 0 or self._maxDP == 0 :

            DPdistrib = {}

            with open (self._VCFFileName, "r") as f:
                VCFline = f.readline()
                m = re.search('^#.*',VCFline)
                while(m):
                    VCFline = f.readline()
                    m = re.search('^#.*', VCFline)


                if self._VCFFSource == "nvc" :

                    while VCFline:
                        DPsum = 0
                        for m in re.finditer('[ATGC]=(\d+),', VCFline):
                            DPsum += int(m.group(1))
                        if DPsum in DPdistrib :
                            DPdistrib[DPsum] += 1
                        else :
                            DPdistrib[DPsum] = 1
                        VCFline = f.readline()


                elif self._VCFFSource == "freebayes" or self._VCFFSource == "mpileup" :
                    while VCFline:
                        for m in re.finditer('DP=(\d+)', VCFline) :
                            DP = int(m.group(1))
                        if DP in DPdistrib :
                            DPdistrib[DP] +=1
                        else :
                            DPdistrib[DP] = 1
                        VCFline = f.readline()



                else :
                    self._logAndRaise("unknown VCF source (supported softwares : Freebayes, MPileup, Naive VaraintCaller")

                median = self.medianFromDic(DPdistrib)

                obsDP =  self.SearchLocalMax(DPdistrib, median)

                ecartType = self.calculEcartType(DPdistrib, obsDP)

                minDP = max(3, obsDP - (2*ecartType))
                if self._minDP == 0 :
                    self._minDP = minDP

                maxDP = obsDP + (2*ecartType)

                if self._maxDP == 0 :
                    self._maxDP = maxDP

            f.close()
            if self._doGraphs :
                self.generateGraphsDistrib(DPdistrib, minDP, maxDP, median, obsDP, ecartType)
            
            self._log.debug("minDP = %d ; maxDP = %d" % (minDP, maxDP))
        else :
            self._log.debug("minDP set as %d ; maxDP set as %d ; skip DP analysis" % (self._minDP, self._maxDP))

        return (self._minDP, self._maxDP)

    def generateGraphsDistrib(self, DPdistrib, minDP, maxDP, median, obsDP, ecartType):
        if DPdistrib.keys() == []:
            self._logAndRaise("dictionary is empty")
        else :
            DPlist =[]
            DPvalues=[]
            for key in DPdistrib.keys():
                DPlist.append(key)
            DPlist.sort()
            for val in DPlist:
                DPvalues.append(str(DPdistrib[int(val)]))
            self._log.info("start generating graphs for DPdistribution")
            scriptFileName = "tmpRscript.r"
            graphFileName = "DpDistribution.%s" % self._graphFormat
            self._scriptFileName = os.path.join(self._graphsFolder, scriptFileName)
            os.mkdir(self._graphsFolder)
            output = open(self._scriptFileName, "w")
            if self._graphFormat == "png" :
                rScript = "%s(filename=\"%s\", width=650, height=800, units=\"px\", pointsize=12)\n" % (self._graphFormat,os.path.join(self._graphsFolder, graphFileName))
            else :
                rScript = "%s(\"%s\")\n" % (self._graphFormat,os.path.join(self._graphsFolder, graphFileName))
            rScript += "par(mfrow=c(2,1))\n"
            rScript += "DPval <- c(%s)\n" %", ".join(DPvalues)
            rScript += "barplot(DPval, main=\"DPDistribution (full)\", xlab=\"DP (Depth of coverage)\", ylab=\"SNP count\", names.arg=c(%s))\n" % ", ".join(str(x) for x in DPlist)
            rScript += "mtext(side=3, adj=1,text=paste(\"median = %s \",\"observed DP = %s \", \"range = %s\"))\n" %(median, obsDP, ecartType)
            minVal = self._minDP - (2*ecartType)
            maxVal = self._maxDP + (2*ecartType)
            rScript += "DPvalreduced <- c(%s)\n" %",".join(DPvalues[minVal:maxVal])
            rScript += "valX<-barplot(DPvalreduced, main=\"DPDistribution (scaled and centered)\", xlab=\"DP (Depth of coverage)\", ylab=\"SNP count\", names.arg=c(%s))\n" % ", ".join(str(x) for x in DPlist[minVal:maxVal])
            rScript += "abline(v=valX[%s], col=\"green\")\n" %(self._minDP-minVal)
            rScript += "abline(v=valX[%s], col=\"green\")\n" %(self._maxDP-minVal)
            rScript += "abline(v=valX[%s], col=\"red\")\n" %(obsDP-minVal) 
            rScript += "mtext(side=3, adj=1,text=paste(\"median = %s \",\"observed DP = %s \", \"range = %s\"))\n" %(median, obsDP, ecartType)
            if (obsDP - ecartType < 2 and obsDP<25) : 
                rScript += "mtext(side=1, col=\"red\", cex = 1.2,  adj=1,text=paste(\"DP too small for a optimal filtering, A DP of at least 25 is recommended\"))\n"
            output.write(rScript)
            output.close()
            self.launchRscript()
            ###########os.remove(self._scriptFileName)
    
    def launchRscript(self):
        cmd ="Rscript %s" %(self._scriptFileName)
        self._log.info("generating graphs...")
        self._log.debug("start command %s"%cmd)
        os.system(cmd)
        
    def filtering(self):
        currentChrom = "unknown"
        
        minQual = self._qual
        lineNumber = 0
        out = open(self._outFile,'w')
        with open (self._VCFFileName, "r") as f:
            VCFline = f.readline()
            m = re.search('^#.*',VCFline)
            addHeader = 1
            while(m):
                lineNumber += 1
                if addHeader == 1 and re.match('^##INFO', VCFline) :

                    out.write("##DetectedFormat=%s\n" % (self._VCFFSource))
                    out.write("##FILTER=<ID=G_AN,Description=\"The SNP has been filtered ; out of AN range(over %s)\">\n"%(self._AN))
                    out.write("##FILTER=<ID=G_AF,Description=\"The SNP has been filtered ; out of AF range(under %s)\">\n"%(self._AF))
                    out.write("##FILTER=<ID=G_DP,Description=\"The SNP has been filtered ; out of DP range(%s - %s)\">\n"%(self._minDP, self._maxDP))
                    out.write("##FILTER=<ID=InDel,Description=\"The SNP has been filtered ; InDel detected\">\n")
                    out.write("##FILTER=<ID=Nmatch,Description=\"The SNP has been filtered ; reference base detected : N\">\n")
#                    out.write("##FILTER=<ID=Qual,Description=\"The SNP has been filtered ; out of Quality range(under %s)\">\n" %(minQual))
                    for (val,File) in enumerate(self._lBedFiles):
                        out.write("##FILTER=<ID=File%d,Description=\"The position has been filtered because it was in a bed file named %s\">\n"%(val+1,File))
                        
                    out.write("##INFO=<ID=G_AN,Number=1,Type=Integer,Description=\"Total number of alleles calculated by VCFFiltering\">\n")
                    out.write("##INFO=<ID=G_AF,Number=1,Type=Float,Description=\"frequency of the most supported alternative allele calculated by VCFFiltering\">\n")
                    out.write("##INFO=<ID=G_Base,Number=1,Type=String,Description=\"base of the most supported alternative allele found by VCFFiltering\">\n")
                    out.write("##INFO=<ID=G_DP,Number=1,Type=Integer,Description=\"total depth calculated by VCFFiltering\">\n")
                    
                    addHeader = 0
                out.write(VCFline)
                VCFline = f.readline()
                m = re.search('^#.*', VCFline)

            while VCFline :
                
                lineNumber += 1
                VCFline = VCFline.rstrip('\n')
                lVCFInfos = VCFline.split("\t")
                if len(lVCFInfos)< 10: #trop court pour etre un VCF
                    self._logAndRaise("VCF file is too short at line %d" % lineNumber)

                if len(lVCFInfos)> 10: #trop long pour etre un VCF ; plusieurs souches ?
                    self._logAndRaise("VCF file is too long at line %d" % lineNumber)

                else :
                    VCFFilters = []
                    AN = 0
                    DP = 0
                    AF = 0.0
                    base = ""
                    bases = lVCFInfos[4].split(",")
                    lVCF3 = lVCFInfos[3].split(",")

                    for b in bases + lVCF3:
                        if len(b)>1:
                            VCFFilters.append("InDel")
                            break

                    if lVCFInfos[3]== "N" : # mappe sur un N
                        VCFFilters.append("Nmatch")

#                    if lVCFInfos[5] == "." : #pas d'information sur le score
#                        pass
#                    elif Decimal(lVCFInfos[5])< minQual : # qualite
#                        VCFFilters.append("Qual < %d" % minQual)

                    if self._VCFFSource == "nvc" :
                        DPspec = 0
                        for m in re.finditer('([ATGC])=(\d+),', lVCFInfos[9]):
                            AN += 1
                            DP += int(m.group(2))
                            for currentBase in bases:
                                if currentBase == m.group(1):
                                    if DPspec < float(m.group(2)):
                                        DPspec = float(m.group(2))
                                        base = currentBase

                            AF = DPspec/DP

                    elif self._VCFFSource == "freebayes" :
                        for m in re.finditer('DP=(\d+)', lVCFInfos[7]):
                            DP = float(m.group(1))
                            if self._stats : 
                                print("%s %s %s" % ( lVCFInfos[0], lVCFInfos[1], DP))
                        for m in re.finditer(';AN=(\d+)', lVCFInfos[7]):
                            AN = float(m.group(1))
                        if lVCFInfos[4] == "." :
                            RO = 0.0
                            for m in re.finditer(';RO=([\d,]+);', lVCFInfos[7]):
                                RO = float(m.group(1))
                            if DP == 0 : 
                                AF = 0 
                            else :
                                AF = (DP-RO)/DP

                        else :
                            AO = 0.0
                            for m in re.finditer(';AO=([\d,]+);', lVCFInfos[7]):
                                lAObases = m.group(1).split(",")

                                for index,currentBase in enumerate(bases):
                                    lAObases[index] = float(lAObases[index])
                                    if lAObases[index] > AO :
                                        AO = lAObases[index]
                                        base = currentBase
                            if DP == 0 : 
                                AF = 0 
                            else :
                                AF = AO/DP

                    elif self._VCFFSource == "mpileup" :
                        for m in re.finditer(';DP=(\d+)', lVCFInfos[7]):
                            DP = float(m.group(1))
                        for m in re.finditer('AN=(\d+)', lVCFInfos[7]):
                            AN = float(m.group(1))
                        for m in re.finditer('DP4=(\d+),(\d+),(\d+),(\d+),', lVCFInfos[7]):
                            AF = (float(m.group(3))+float(m.group(4)))/DP

                        pass

                    if AN > self._AN :
                        VCFFilters.append("G_AN")
                    if lVCFInfos[4] == "." :
                        base=lVCFInfos[3]
                        if AF > 1-self._AF :
                            VCFFilters.append("G_AF")
                    else :
                        if AF < self._AF :
                            VCFFilters.append("G_AF")
                    if DP < self._minDP or DP > self._maxDP :
                        VCFFilters.append("G_DP")
                        
                    
                    if len(self._lBedFiles) > 0:
                        if currentChrom != lVCFInfos[0] :
                            self._getfilteredPositionsOfCurrentChromosome(lVCFInfos[0])
                            currentChrom = lVCFInfos[0]
                        lBedFilters = self._PositionToExclude(lVCFInfos[0],lVCFInfos[1])
                        lBedFilters = ["File%d" % i for i in lBedFilters]
                        VCFFilters.extend(lBedFilters)


                    VCFGandalfInfos = ("G_AN=%d;G_AF=%.2f;G_DP=%d;G_Base=%s" %(AN, AF, DP, base) )
                    lVCFInfos[7] = ";".join([lVCFInfos[7], VCFGandalfInfos])

                    if len(VCFFilters)!=0 :
                        lVCFInfos[6]=";".join(VCFFilters)
                    out.write("\t".join(lVCFInfos))
                    out.write("\n")
                VCFline = f.readline()
        f.close()
        out.close()




    def _readBedFilesSQLLite(self):
        
        if len(self._lBedFiles)>0 :
            self.conn = sqlite3.connect(self.db)
            self._log.debug("Opened database successfully")
            
            self.conn.execute('''CREATE TABLE BED
                           (ID INT PRIMARY KEY NOT NULL,
                           CHROM TEXT NOT NULL,
                           POS   INT NOT NULL,
                            SOURCE        INT);''')
            self._log.debug("Table created successfully")
            
            fileNb = 0
            id = 0
            for File in self._lBedFiles:
                fileNb += 1
                with open (File, "r") as f:
                    Bedline = f.readline()
                    while Bedline :
                        Bedline = Bedline.rstrip('\n')
                        lBedInfos = Bedline.split("\t")
                        if len(lBedInfos) > 1:
                            start = int(lBedInfos[1])+1
                            stop = int(lBedInfos[2])+1
                            for pos in range(start, stop):
                                id += 1
                                self.conn.execute("INSERT INTO BED (ID,CHROM,POS,SOURCE) \
                                          VALUES ('%d', '%s', %d, %d)" %(id, lBedInfos[0], pos, fileNb));
                                          
                                if id %300000 == 0:
                                    self.conn.commit()
                        Bedline = f.readline()
                f.close()
            self.conn.commit()
            
                        
    def _getfilteredPositionsOfCurrentChromosome(self, chrom):
        cursor = self.conn.execute("SELECT ID, CHROM, POS, SOURCE  from BED where CHROM = '%s'" % chrom)
        self._dExcludedPositions = {}
        for row in cursor:
            if "%s-%d" % (row[1],row[2]) in self._dExcludedPositions :
                self._dExcludedPositions["%s-%d" % (row[1],row[2])].append(row[3])
            else:
                self._dExcludedPositions["%s-%d" % (row[1],row[2])]= [row[3]]
                            
        

        
    def _readBedFiles(self):
        self._dExcludedPositions = {}
        fileNb = 0
        for File in self._lBedFiles:
            fileNb += 1
            with open (File, "r") as f:
                Bedline = f.readline()
                while Bedline :
                    Bedline = Bedline.rstrip('\n')
                    lBedInfos = Bedline.split("\t")
                    if len(lBedInfos) > 1:
                        start = int(lBedInfos[1])+1
                        stop = int(lBedInfos[2])+1
                        for pos in range(start, stop):
                            key = "%s-%d" % (lBedInfos[0], pos)
                            if key in self._dExcludedPositions:
                                self._dExcludedPositions[key].append("File%d" % fileNb)
                            else:
                                self._dExcludedPositions[key]= ["File%d" % fileNb]
                    Bedline = f.readline()
            f.close()


    def _PositionToExclude(self, Chrom, Pos):
        key = "%s-%d" % (Chrom,int(Pos))
        if key in self._dExcludedPositions:
            return self._dExcludedPositions[key]
        else:
            return []
        
    def _removeSQLLitedb(self):
        if len(self._lBedFiles)>0 :
            os.remove(self.db)

    def run(self):
        self.checkoption()
        self.getSource()
        self.analyseDPDistribution()
#        self._readBedFiles()
        self._readBedFilesSQLLite()
        self.filtering()
        self._removeSQLLitedb()
        pass

if __name__== "__main__":
    iVCFFiltering = VCFFiltering()
    iVCFFiltering.setAttributesFromCmdLine()
    iVCFFiltering.run()


