#!/usr/bin/env python

# Copyright INRA (Institut National de la Recherche Agronomique)
# http://www.inra.fr
# http://urgi.versailles.inra.fr
#
# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

# regarder de pres a self._storefastaFile()
# & self._createtempGenomeCoordFile : ils utilisent
# tout les deux la meme biseqDB ; on peut probablement
# les associer et ne faire qu'une instance de bsd


from commons.core.LoggerFactory             import LoggerFactory
from commons.core.utils.RepetOptionParser   import RepetOptionParser
from commons.core.utils.FileUtils           import FileUtils
from commons.core.seq.BioseqDB              import BioseqDB
from commons.core.checker.RepetException    import RepetException
import shutil
import os
import tempfile

LOG_NAME = "gnome_tools"

class VCFStorage(object):

    def __init__(self, fastaFileName = "", genomeListFileName="", tableName = "", outFileName="",confFileName="", workDir = "", doClean = False, verbosity = 0):
        self._fastaFileName = fastaFileName
        self._genomeListFileName = genomeListFileName
        self._outFileName = outFileName
        self._workdir = workDir
        self._doClean = doClean
        self._verbosity = verbosity
        self._buffSizefusion = 1000000

        self._lTmpVariantsFiles = []
        self._tempGenomeCoordFile = "genome.List"
        self._log = LoggerFactory.createLogger("%s.%s" % (LOG_NAME, self.__class__.__name__), self._verbosity)

    def setAttributesFromCmdLine(self):
        self._toolVersion = "1.2"
        description = "VCFStorage version %s" % self._toolVersion
        epilog = "\nStore info from variant calling into a table. It will create a tabulate file with the different infos\n"
        epilog += "example 1 : VCFStorage -f fasta.fa -l genomelist.list -c -w workdir -o output.tab \n"
        parser = RepetOptionParser(description = description, epilog = epilog, version= self._toolVersion)
        parser.add_option("-f", "--fasta",        dest = "fastaFile",      action = "store",     type = "string", help = "Input fasta file name [compulsory] [format: Fasta]",                default = "")
        parser.add_option("-l", "--genomelist",   dest = "genomeListFile", action = "store",     type = "string", help = "Input list of genome/vcf file name [compulsory] [format: tab]",     default = "")
        parser.add_option("-w", "--workDir",      dest = "workDir",        action = "store",     type = "string", help = "name of the workingDirectory",                                      default = "")
        parser.add_option("-o", "--out",          dest = "outFile",        action = "store",     type = "string", help = "Output file name [compulsory] [format: tab]",                       default = "")
        parser.add_option("-c", "--clean",        dest = "doClean",        action = "store_true",                 help = "Clean temporary files [optional] [default: False]",                 default = False)
        parser.add_option("-v", "--verbosity",    dest = "verbosity",      action = "store",     type = "int",    help = "Verbosity [optional] [default: 3]",                                 default = 3)
        options = parser.parse_args()[0]
        self._setAttributesFromOptions(options)

    def _setAttributesFromOptions(self, options):
        self.setInputFastaFile(options.fastaFile)
        self.setInputGenomeListFile(options.genomeListFile)
        self.setOutFileName(options.outFile)
        self.setWorkDir(options.workDir)
        self.setDoClean(options.doClean)
        self.setVerbosity(options.verbosity)

    def setInputFastaFile(self, fastaFileName):
        self._fastaFileName = fastaFileName

    def setInputGenomeListFile(self, genomeListFileName):
        self._genomeListFileName = genomeListFileName

    def setOutFileName(self, outFile):
        self._outFileName = outFile

    def setWorkDir(self, workDir):
        self._workdir = workDir

    def _createWorkDir(self):
        if not FileUtils.isRessourceExists(self._workdir):
            try:
                os.mkdir(self._workdir)
            except IOError as e:
                e.message("cannot create working directory")

    def _cleanWorkDir(self):
        if self._doClean:
            shutil.rmtree(self._workdir)

    def setDoClean(self, doClean):
        self._doClean = doClean

    def setVerbosity(self, verbosity):
        self._verbosity = verbosity

    def _logAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        raise RepetException(errorMsg)
    
    def _CleanLogAndRaise(self, errorMsg):
        self._log.error(errorMsg)
        if self._doClean:
            if FileUtils.isRessourceExists(self._workdir):
                shutil.rmtree(self._workdir)
            if FileUtils.isRessourceExists(self._outFileName):
                os.remove(self._outFileName)
        raise RepetException(errorMsg)

    def _checkOptions(self):
        if self._workdir == "":
            self._logAndRaise("No workdir specified !")
        elif FileUtils.isRessourceExists(self._workdir):
            self._logAndRaise("directory %s found, cannot be used as workdir. Please delete it or choose an another working directory." % self._workdir)
        
        if self._fastaFileName == "":
            self._logAndRaise("Missing input fasta file")
        else:
            if FileUtils.isRessourceExists(self._fastaFileName):
                self._fastaFileName = os.path.abspath(self._fastaFileName)
            else:
                self._logAndRaise("Input Fasta file '%s' does not exist!" % self._fastaFileName)

        if self._outFileName == "":
            self._logAndRaise("Missing output filename")
        else:
            if FileUtils.isRessourceExists(self._outFileName):
                #self._logAndRaise("output file %s already exist!" % self._outFileName)
                pass

        if self._genomeListFileName == "":
            self._logAndRaise("Missing input genome/vcf/bam list file")
        else:
            if FileUtils.isRessourceExists(self._genomeListFileName):
                self._genomeListFileName = os.path.abspath(self._genomeListFileName)
            else:
                self._logAndRaise("Input reference genome/vcf/bam list file '%s' does not exist!" % self._genomeListFileName)

        genomeListFile = open(self._genomeListFileName,'r')

        for genomeInfos in genomeListFile:
            if genomeInfos.startswith("#"):
                pass
            else :
                genomeInfos = genomeInfos.rstrip('\n')
                (genomeName, genomeVCF)= genomeInfos.split("\t")
                if FileUtils.isRessourceExists(genomeVCF):
                    pass
                else:
                    self._logAndRaise("file '%s' does not exist! (VCF file indicated on file %s, for genome %s" % (genomeVCF, self._genomeListFileName, genomeName))

    def _storeFastaFile(self):
        self._GenomeSize = 0
        iBioseqDB = BioseqDB(self._fastaFileName)
        if self._outFileName  != "":
            output = open(self._outFileName, "w")

        header = "CHROM\tPOS\treference\n"
        output.write(header)

        for chrom in iBioseqDB.db:
            self._log.info("reference genome, chromosome %s" % chrom.header)
            buffSize = 1000000
            position = 0
            size = chrom.getLength()
            self._GenomeSize += size
            start = 0
            end = min(buffSize,size)
            while end <= size:
                for code in chrom.sequence[start:end]:
                    code = code.upper()
                    position += 1
                    output.write("%s\t%d\t%s\n" %  (chrom.header, position, code))
                if end == size:
                    break
                else:
                    start = end
                    end = min(end + buffSize, size)
        output.close()
        self._log.info("writing reference genome variant in output file %s " % self._outFileName)
        
    def _StoreIndividuals(self): # scanne les genomes et insere en base
        genomeListFile = open(self._genomeListFileName,'r')
        
        for genomeInfos in genomeListFile:
            if genomeInfos.startswith("#"):
                pass
            else : 
                genomeInfos = genomeInfos.rstrip('\n')
                (genomeName, genomeVCF)= genomeInfos.split("\t")
                self._log.debug("start %s" % genomeName)
                self._log.info( "%s %s "%(genomeName, genomeVCF))
                self._VCFFiletoOutput(genomeName, genomeVCF)
                
        genomeListFile.close()
        
    def _VCFFiletoOutput(self, genomeName, genomeVCF) :
        self._log.info("start %s" % genomeName)
        fTmp = tempfile.NamedTemporaryFile(dir = self._workdir, delete = False)
        GenomeTable = open(self._outFileName, "r")
        
        VCFFile = open(genomeVCF, "r")
        VCFline = VCFFile.readline()
        while VCFline.startswith("#"):
            VCFline = VCFFile.readline()
            
        lVCF = VCFline.rstrip().split("\t")
        
        
        fTmp.write("%s\t%s\n" % (GenomeTable.readline().rstrip(), genomeName))
        for line in GenomeTable:
            line = line.rstrip()
            lGenomeInfos = line.split("\t")
            if lVCF[0] :
                if (lGenomeInfos[0],lGenomeInfos[1]) == (lVCF[0],lVCF[1]):
                    if lVCF[6] != "." and lVCF[6] != "PASS" :
                        fTmp.write("%s\tF\n" % line)
                    elif lVCF[4] == ".":
                        fTmp.write("%s\tR\n" % line)
                    else :
                        fTmp.write("%s\t%s\n" % (line,lVCF[4]))
                
                    lVCF = VCFFile.readline().rstrip().split("\t")
                else :
                    fTmp.write("%s\tU\n" % line)
            else :
                fTmp.write("%s\tU\n" % line)
        
        os.rename(fTmp.name, self._outFileName)
        self._log.info("%s done" % genomeName)
    
    def run(self):
        self._checkOptions()
        self._createWorkDir()
        self._storeFastaFile()
        self._StoreIndividuals()
        self._cleanWorkDir()
        pass



if __name__ == "__main__":
    iVCFStorage = VCFStorage()
    iVCFStorage.setAttributesFromCmdLine()
    iVCFStorage.run()
